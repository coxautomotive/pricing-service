var seneca = require('seneca')();
var async = require('async');

seneca.client({host: 'salestaxsvc', pins: [{role: 'salestax', cmd: 'getById'}]});
seneca.client({host: 'shippingsvc', pins: [{role: 'shipping', cmd: 'getById'}]});

seneca.add({role: 'pricing', cmd: 'getById'}, function(msg, respond) {
  async.parallel([
    function(callback) {
      seneca.act({role: 'salestax', cmd: 'getById', id: 1234}, callback);
    },
    function(callback) {
      seneca.act({role: 'shipping', cmd: 'getById', id: 1234}, callback);
    }],
    function(err, results) {
      var price = {
        base: 41.18
      };
      price.salestax = results[0].salestax;
      price.shipping = results[1].shipping;
      respond(null, price);
    });
});

seneca.listen();

seneca.ready(function() {
  console.log('pricing-service ready!');
});
